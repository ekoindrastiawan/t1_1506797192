package com.example.controller;

import com.example.model.PesertaModel;
import com.example.service.PesertaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestPesertaController {
    
    @Autowired
    PesertaService pesertaDAO;
    
    @RequestMapping(value = "/rest/peserta", method = RequestMethod.GET)
    public PesertaModel detailPeserta(Model model,
            @RequestParam(value = "nomor", required = false) String nomor) {

        PesertaModel peserta = pesertaDAO.selectPeserta(nomor);
        model.addAttribute("peserta", peserta);
        if (peserta == null) {
            model.addAttribute("nomor", nomor);
            return null;
        } else {
            Integer umur = pesertaDAO.getAge(nomor);
            model.addAttribute("umur", umur);
            return peserta;
        }

    }
    
}
