package com.example.controller;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;
import com.example.service.UnivService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestProdiController {

    @Autowired
    UnivService univDAO;

    @Autowired
    ProdiService prodiDAO;

    @Autowired
    PesertaService pesertaDAO;

    @RequestMapping(value = "/rest/prodi", method = RequestMethod.GET)
    public ProdiModel detailProdi(Model model,
            @RequestParam(value = "kode", required = true) String kodeProdi) {

        ProdiModel prodi = prodiDAO.selectProdi(kodeProdi);
        model.addAttribute("prodi", prodi);
        if (prodi == null) {
            model.addAttribute("errorMessage", "notfound");
            model.addAttribute("kodeProdi", kodeProdi);
            return null;
        } else {
            List<PesertaModel> daftarPeserta = prodiDAO.selectPesertas(kodeProdi);
            model.addAttribute("daftarPeserta", daftarPeserta);
            UnivModel univ = univDAO.selectUniv(prodi.getKode_univ());
            model.addAttribute("univ", univ);
            // peserta termuda dan tertua
            String msg = "0";
            Integer age = 0;
            Integer youngest = 0;
            Integer oldest = 0;
            if (daftarPeserta.isEmpty()) {
                msg = "0";
            } else if (daftarPeserta.size() == 1) {
                msg = "1";
                age = pesertaDAO.getAge(daftarPeserta.get(0).getNomor());
                model.addAttribute("age", age);
            } else if (daftarPeserta.size() > 1) {
                msg = "2";
                Integer countDaftarPeserta = prodiDAO.countSelectPesertaByProdi(kodeProdi);
                model.addAttribute("countDaftarPeserta", countDaftarPeserta);
                oldest = pesertaDAO.getAge(daftarPeserta.get(0).getNomor());
                model.addAttribute("oldest", oldest);
                youngest = pesertaDAO.getAge(daftarPeserta.get(countDaftarPeserta - 1).getNomor());
                model.addAttribute("youngest", youngest);
            }
            model.addAttribute("msg", msg);
            return prodi;
        }

    }

}
